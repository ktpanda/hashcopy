#!/usr/bin/python3
import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('-f', '--force', action='store_true', help='Force run even with uncommitted changes')
    p.add_argument('-v', '--new-version', help='Override the new version')
    args = p.parse_args()

    os.chdir(Path(__file__).parent)

    if not args.force:
        p = subprocess.run(['git', 'status', '--porcelain', '-uno'], check=True, encoding='utf8', stdout=subprocess.PIPE)
        if p.stdout:
            print('Uncommitted changes:')
            print(p.stdout)
            return

        p = subprocess.run(['git', 'log', '-n', '1', '--pretty=%B'], check=True, encoding='utf8', stdout=subprocess.PIPE)
        last_commit_message = p.stdout.strip()
        if re.match('^Version \d+\.\d+\.\d+', last_commit_message):
            print(f'Last commit was a version bump ({last_commit_message})')
            return

    old_version = None
    new_version = None
    version_parts = None
    if args.new_version:
        new_version = args.new_version
        version_parts = [int(v) for v in new_version.split('.')]

    git_add_args = []
    cfgpath = Path('pyproject.toml')

    text = cfgpath.read_text()
    m = re.search(r'version\s*=\s*"(.*?)"', text)

    old_version = m.group(1)

    if new_version is None:
        version_parts = [int(v) for v in old_version.split('.')]
        version_parts[-1] += 1
        new_version = '.'.join(str(v) for v in version_parts)

        print(f'new version = {new_version}')

    new_text = text[:m.start(1)] + new_version + text[m.end(1):]
    if text != new_text:
        cfgpath.write_text(new_text)
        git_add_args.append(cfgpath)

    if old_version is None:
        print('Could not find version in any config files!')
        return 1

    if git_add_args:
        subprocess.run(['git', 'add'] + git_add_args, check=True)
    subprocess.run(['git', 'commit', '-m', f'Version {new_version}'], check=True)
    new_tag = f'version_{version_parts[0]}_{version_parts[1]:02d}_{version_parts[2]:04d}'
    if args.force:
        subprocess.run(['git', 'tag', '-f', new_tag], check=True)
    else:
        subprocess.run(['git', 'tag', new_tag], check=True)

    return 0

if __name__ == '__main__':
    sys.exit(main())
